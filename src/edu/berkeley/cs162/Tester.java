package edu.berkeley.cs162;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class Tester {
	
	String host = "";
	String port = "";
	boolean randomize = false;
	static String  testFile = "";
	
	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.begin(args);  
	}

	private void begin(String[] cArgs) {

		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        Document doc=null;
        testFile = cArgs[0];
        try {
        	doc = docBuilderFactory.newDocumentBuilder().parse(cArgs[0]);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Node hostXMLNode=doc.getDocumentElement().getFirstChild().getNextSibling();
		host = hostXMLNode.getTextContent();
		Node portXMLNode=hostXMLNode.getNextSibling().getNextSibling();
		port = portXMLNode.getTextContent();
		Node randomizeXMLNode=portXMLNode.getNextSibling().getNextSibling();
		if (randomizeXMLNode.getTextContent().equals("true"))
			randomize=true;
		
		Node requestsXMLRoot = randomizeXMLNode.getNextSibling().getNextSibling();
		NodeList allXMLRequests = requestsXMLRoot.getChildNodes();
		for (int i=0 ; i<allXMLRequests.getLength(); i++)
		{
			Node requestNode= allXMLRequests.item(i);
			if(requestNode.getNodeType()==Node.TEXT_NODE){
				continue;
			}
			if(requestNode.getNodeName().equals("SINGLE")){
				executeSingle(requestNode);
			} else if(requestNode.getNodeName().equals("MULTIPLE")) {
				executeMultiple(requestNode);
			}
		}
	}

	private void executeMultiple(Node rootXMLMultipleRequests) {
		LinkedList<KVClientRunner> singleKVClientRunners = new LinkedList<KVClientRunner>();
		NodeList singleXMLnodes = rootXMLMultipleRequests.getChildNodes();
		for (int i=0 ; i<singleXMLnodes.getLength(); i++)
		{
			Node tempNode = singleXMLnodes.item(i);
			if(tempNode.getNodeType()==Node.TEXT_NODE){
				continue;
			}
			singleKVClientRunners.add(new KVClientRunner(tempNode));
		}
		for (int i=0;i<singleKVClientRunners.size();i++) {
			singleKVClientRunners.get(i).start();
		}
	}

	private void executeSingle(Node requestNode) {
		KVClientBuilder cl = new KVClientBuilder(requestNode);
		cl.executeRequest();		
	}

	private class KVClientRunner extends Thread {
		
		Node myRequestedNode = null;
		KVClientBuilder cl = null;
		public KVClientRunner(Node requestedNode) {
			myRequestedNode=requestedNode;
			cl = new KVClientBuilder(myRequestedNode);
		}
		
		public void run() {		
			cl.executeRequest();
		}
	}
	
	private class KVClientBuilder {
		
		private String requestType = "";
		private String key = "";
		private String value = "";
		private String expected = "";
		private KVClient kvC= null;
		
		public KVClientBuilder(Node requestNode) {
			requestType = ((Element) requestNode).getElementsByTagName("REQUESTTYPE").item(0).getTextContent();
			key = ((Element) requestNode).getElementsByTagName("KEY").item(0).getTextContent();
			value = ((Element) requestNode).getElementsByTagName("VALUE").item(0).getTextContent();
			expected = ((Element) requestNode).getElementsByTagName("EXPECTED").item(0).getTextContent();
			kvC = new KVClient(host, Integer.parseInt(port));
		}
		
		public void executeRequest() {
			try {

				if (requestType.equals("get")) {
					System.out.println("get got: " + (String) kvC.get(key));
				} else if (requestType.equals("put")) {
					boolean putRet = kvC.put(key,value);
//					if(String.valueOf(putRet).equals(expected)) {
//						System.out.println(Tester.testFile + " PASSED. Request was put with key: " + key + " and value:" + value + "expected: " + expected + " but got: " + String.valueOf(putRet));
//					} else {
//						System.out.println(Tester.testFile + " FAILED. Request was put with key: \"" + key + "\" and value: \"" + value + "\" expected: " + expected + " but got: " + String.valueOf(putRet));
//					}
				} else {
					kvC.del(key);
				}
			}	catch (KVException e) {
				e.printStackTrace();
			}
		}
	}
}
