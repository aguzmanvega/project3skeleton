package edu.berkeley.cs162;

import java.net.*;
import java.io.*;



public class PingPongServer{

    ServerSocket server;

    public PingPongServer() throws IOException{
	server = new ServerSocket();
	SocketAddress add = new InetSocketAddress("ec2-23-21-31-43.compute-1.amazonaws.com",8081);
	server.bind(add);
	System.out.println("server up on 8081");
    }

    public void run(){
	Socket s = null;
	while(true){
	    try{
	    s = server.accept();
	    } catch(Exception e) { e.printStackTrace();}
	    if(s!=null)
		new PingPongWorker(s).start();
	}
    }

    public static void main(String[] args){
	PingPongServer pps = null;
	try{
	pps = new PingPongServer();
	}catch(Exception e){e.printStackTrace();}
	pps.run();
    }
}

class PingPongWorker extends Thread{
    Socket s;
    public PingPongWorker(Socket s){
	this.s = s;
    }

    public void run(){
	try{
	PrintWriter out = new PrintWriter(s.getOutputStream(),true);
	out.print("pong");
	out.close();
	s.close();
	}catch(IOException e){e.printStackTrace();}
    }
}