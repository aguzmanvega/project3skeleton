package edu.berkeley.cs162;

import java.util.HashMap;
import java.util.Random;

public class LocalTest{
    public static int level = 0;
    public static String host;
    public static int port = 8080;

    public static void main(String[] args){
	Integer level = Integer.parseInt(args[0]);
	LocalTest.host = args[1];
	String host = args[1];
	int port = LocalTest.port;
	if(level == 0){ //one client
	    try{
	    KVClient c1 = new KVClient<String,String>(host,port);
	    //System.out.println("getting key not there");
	    //System.out.println(c1.get("keynothere"));
	    c1.put("sfd","val");
	    System.out.println("putting key1 in store... returns " + c1.put("key1","value1"));
	    System.out.println("getting key1 from store ... returns " + c1.get("key1"));
	    c1.del("key1");
	    System.out.println("deleting key1 from store ");
	    System.out.println("getting key1 from store should be removed ... returns " + c1.get("key1"));
	    }catch(Exception e) {e.printStackTrace();}
	}
	if(level == 1){ //two clients with strings
	    try{
	    KVClient c1 = new KVClient<String,String>(host,port);
	    KVClient c2 = new KVClient<String,String>(host,port);
	    System.out.println("putting key1 in store... returns " + c1.put("key1","value1"));
	    System.out.println("putting key2 in store... returns " + c2.put("key2","value2"));
	    System.out.println("getting key1 from store ... returns " + c1.get("key1"));
	    c1.del("key1");
	    System.out.println("deleting key1 from store ");
	    System.out.println("getting key1 from store should be removed ... returns " + c1.get("key1"));
	    System.out.println("getting key2 from store ... returns " + c1.get("key2"));
	    System.out.println("deleting key2 from store ");
	    c1.del("key2");
	    System.out.println("getting key2 from store should be removed ... returns " + c1.get("key2"));
	    }catch(Exception e) {e.printStackTrace();}
	}
	if(level == 2){ // testing a serialized HashMap
	    try{
	    HashMap h = new HashMap();
	    h.put(1,"one");
	    h.put(2,"two");
	    KVClient c1 = new KVClient<String,HashMap>(host,port);
	    c1.put("hashmap",h);
	    HashMap ret = (HashMap)c1.get("hashmap");
	    System.out.println("Should be one:  " + ret.get(1));
	    System.out.println("Should be two:  " + ret.get(2));
	    }catch(Exception e){e.printStackTrace();}
	    
	}
	if(level == 3){ //10 threads simultaneously
	    for(int i = 0; i<10;i++){
		new ClientThread(i,false).start();
	    }
	}
	if(level == 4){ // 20 threads random order
	    for(int i = 0; i<20; i++){
		new ClientThread(i,true).start();
	    }
	}
	if(level == 5){
	    KVClient<String,String> c1 = null;
	    try{
	    c1 = new KVClient<String,String>(LocalTest.host,LocalTest.port);
	    
	    String val = null;
	    for(Integer i = 0; i<10;i++){
		val = c1.get("key"+i.toString());
		System.out.println("got  " + val);
	    }
	    }catch(Exception e){e.printStackTrace();}
	}
		
	
    }
}

class ClientThread extends Thread{
    String id;
    boolean randomFlag;
    static Random r = new Random();

    public ClientThread(Integer id,boolean randomFlag){
	this.id = id.toString();
    }

    public void run(){
	try{
	if(randomFlag) Thread.sleep((int)(r.nextFloat()*2000));
	KVClient c1 = new KVClient<String,String>(LocalTest.host,LocalTest.port);
	c1.put("key"+id,"value"+id);
	String val = null;
	if(!(val = (String)c1.get("key"+id)).equals("value"+id)) System.out.println("Thread " +id+ " failed to get the right value back --- got " + val + " instead");
	c1.put("key2"+id,"value2"+id);
	if(!((String)c1.get("key2"+id)).equals("value2"+id)) System.out.println("Thread " +id+" failed to get the right value2 back");
	if(c1.put("key2"+id,"valuenew"+id) != true) System.out.println("Thread " + id+" should have been true on putting key" +id+" with valuenew"+id);
	if(!(val = (String)c1.get("key2"+id)).equals("valuenew"+id)) System.out.println("Thread " +id+ " failed to get the right valuenew back got ---- " +val);
	System.out.println("Thread " +id+ "has finished executing.");
	System.out.println("");
	System.out.println("");
	}catch(Exception e){e.printStackTrace();}
						   
    }
}