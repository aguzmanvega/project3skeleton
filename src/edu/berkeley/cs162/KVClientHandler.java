/**
 * Handle client connections over a socket interface
 * 
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2011, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *    
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.net.Socket;

/**
 * This NetworkHandler will asynchronously handle the socket connections. 
 * It uses a threadpool to ensure that none of it's methods are blocking.
 *
 * @param <K> Java Generic type for the Key
 * @param <V> Java Generic type for the Value
 */
public class KVClientHandler<K extends Serializable, V extends Serializable> implements NetworkHandler {
	private KeyServer<K, V> keyserver = null;
	private ThreadPool threadpool = null;
	
	public KVClientHandler(KeyServer<K, V> keyserver) {
		initialize(keyserver, 12);
	}

	public KVClientHandler(KeyServer<K, V> keyserver, int connections) {
		initialize(keyserver, connections);
	}

	private void initialize(KeyServer<K, V> keyserver, int connections) {
		this.keyserver = keyserver;
		threadpool = new ThreadPool(connections);	
	}
	
	/* (non-Javadoc)
	 * @see edu.berkeley.cs162.NetworkHandler#handle(java.net.Socket)
	 */

	public class SocketDealer implements Runnable {
	    Socket s = null;
	    
	    public SocketDealer(Socket s){
		this.s = s;
	    }

	    public void run() {
		// for later
	    KVMessage msgIn = null;
	    KVMessage msgOut = new KVMessage(null,null,null);  
			
		try { 
			msgIn = new KVMessage(s.getInputStream());
		} catch (KVException e) {
		    msgIn.msgType = "failure";
		    msgOut = e.getMsg();
		} catch (IOException e) {
		    msgIn.msgType = "failure";
		    msgOut.msgType = "error";
		    msgOut.msg = "Network Error: Could not send data";
		}
	    
		
	    String type =  msgIn.getType();
	    String key =   msgIn.getKey();
	    String value =  msgIn.getValue();
	    
	    
	    if (type.matches("getreq")){
	      
	    	try {
		    value = (String) keyserver.get((K) key);
		    msgOut.msgType = "getResp";
		    msgOut.key = key;
		    msgOut.value = value;
		} catch (KVException e) {
		    msgOut = e.getMsg();
		}

	    }
	    
	    if (type.matches("putreq")){
	    	boolean status = false;
		try {
		    status = keyserver.put((K) key, (V) value);
		    msgOut.msgType = "putResp";
		    msgOut.msg = "Success";
		    if (status){msgOut.status = "True";} else {msgOut.status = "False";}
		} catch (KVException e) {
		    msgOut = e.getMsg();
		}

	    }
	   
	  if (type.matches("delreq")){
	      
	      try {
		  keyserver.del((K) key);
		  msgOut.msgType = "delResp";
		  msgOut.msg = "Success";
	      } catch (KVException e) {
		  msgOut = e.getMsg();
	      }
	      
	  } 
	  
	  
	  OutputStream out = null;
	  try {
	      out = s.getOutputStream();
	      PrintWriter writer = new PrintWriter(out,true);
	      writer.println(msgOut.toXML());
	  } catch (IOException e) {
	      e.printStackTrace();
	  }
	  
	  try {
	      out.close();
	  } catch (IOException e) {
	      e.printStackTrace();
	  }
	  try {
	      s.close();
	  } catch (IOException e) {
	      e.printStackTrace();
	  }
	    }
	}
	
	    
	    
	
	
	    
	    
public void handle(Socket client) throws IOException{
	   
	    Runnable r = new SocketDealer(client);
	    try {
	    	
			threadpool.addToQueue(r);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}

	    }



