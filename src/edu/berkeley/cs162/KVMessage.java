/**
 *
 * XML Parsing library for the key-value store
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2011, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.*;

import javax.xml.parsers.*;
import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


/**
 * This is the object that is used to generate messages the XML based messages
 * for communication between clients and servers. Data is stored in a
 * marshalled String format in this object.
 */
public class KVMessage {
	public String msgType = null;
	public String key = null;
	public String value = null;
	public String msg = null;
	public String status = null;

	public KVMessage(String msgType, String key, String value) {
		this.msgType = msgType;
		this.key = key;
		this.value = value;
		
	}
	
	public String toString()
	{
		String s = "type " + msgType + "\n";
		s += "key " + key + "\n";
		s+= "value " +value + "\n";
		s+= "msg " +msg + "\n";
		s+= "status " + status + "\n";
		return s;
	}



	/* Hack for ensuring XML libraries does not close input stream by default.
	 * Solution from http://weblogs.java.net/blog/kohsuke/archive/2005/07/socket_xml_pitf.html */
	private class NoCloseInputStream extends FilterInputStream {
	    public NoCloseInputStream(InputStream in) {
	        super(in);
	    }

	    public void close() {} // ignore close
	}
	
	public String getValue()
	{
		return value;
	}
	
	public String getType()
	{
		return msgType;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public KVMessage(String error)
	{
		msgType = "error";
		msg = error;
	}

	public KVMessage(InputStream input) throws KVException {
		// Need to include javax.xml.parsers.DocumentBuilder
		//System.out.println(convertStreamToString(input));
		BufferedReader is = new BufferedReader(new InputStreamReader(input));
		
		String str = "";
		
		try 
		{
			while(is.ready())
				str += is.readLine() + "\n";
		} 
		catch (IOException e1) {System.out.println("no");}
		
		try
		{
			InputStream test = new ByteArrayInputStream(str.getBytes("UTF-8"));
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document d = db.parse(test);
			NodeList n = d.getElementsByTagName("KVMessage");
			
			Element node = (Element)n.item(0);
			this.msgType = (node.getAttribute("type"));

			try 
			{
				this.key = (node.getElementsByTagName("Key").item(0).getFirstChild().getNodeValue());
			}
			catch(NullPointerException e) {}
			
			try 
			{
				this.value = (node.getElementsByTagName("Value").item(0).getFirstChild().getNodeValue());
			}
			catch(NullPointerException e) {}			
			
			try 
			{
				this.msg = (node.getElementsByTagName("Message").item(0).getFirstChild().getNodeValue());
			}
			catch(NullPointerException e) {}
			
			try 
			{
				this.status = (node.getElementsByTagName("Status").item(0).getFirstChild().getNodeValue());
			}
			catch(NullPointerException e) {}
			
		}
		catch(SAXException e) 
		{
			System.out.println(e);
			throw new KVException(new KVMessage("XML Error: Received unparsable message"));
		}
		catch(Exception e) {}

		// Need to retrieve each element from the document
	}


	/**
	 * Generate the XML representation for this message.
	 * @return the XML String
	 */
	public String toXML() {
		String outputMsg = new String("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		

		// Make sure to use escape characters correctly
		if(msgType.matches("getreq"))
		        outputMsg += "<KVMessage type=\"getreq\">\n<Key>" + key + "</Key>\n</KVMessage>";

		if(msgType.matches("putreq"))
		        outputMsg += "<KVMessage type=\"putreq\">\n<Key>" + key + "</Key>\n<Value>" + value + "</Value>\n</KVMessage>";

		if(msgType.matches("delreq"))
		        outputMsg += "<KVMessage type=\"delreq\">\n<Key>" + key + "</Key>\n</KVMessage>";

		if(msgType.matches("getResp"))
		        outputMsg += "<KVMessage type=\"resp\">\n<Key>" + key + "</Key>\n<Value>" + value + "</Value>\n<Message>Success</Message>\n</KVMessage>";

		if(msgType.matches("putResp"))
		        outputMsg += "<KVMessage type=\"resp\">\n<Key>" + key + "</Key>\n<Status>" + status + "</Status>\n<Message>Success</Message>\n</KVMessage>";

		if(msgType.matches("delResp"))
		        outputMsg += "<KVMessage type=\"resp\">\n<Key>" + key + "</Key>\n<Message>Success</Message>\n</KVMessage>";

		if(msgType.matches("error"))
		        outputMsg += "<KVMessage type=\"resp\">\n<Key>" + key + "</Key>\n<Message>" + msg + "</Message>\n</KVMessage>";

    	return outputMsg;
	}
}
