/**
 * Client component for generating load for the KeyValue store.
 * This is also used by the Master server to reach the slave nodes.
 *
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2011, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;


import sun.misc.*;


/**
 * This class is used to communicate with (appropriately marshalling and unmarshalling)
 * objects implementing the {@link KeyValueInterface}.
 *
 * @param <K> Java Generic type for the Key
 * @param <V> Java Generic type for the Value
 */
public class KVClient<K extends Serializable, V extends Serializable> implements KeyValueInterface<K, V> {

	private String server = null;
	private int port = 0;

	/**
	 * @param server is the DNS reference to the Key-Value server
	 * @param port is the port on which the Key-Value server is listening
	 */
	public KVClient(String server, int port) 
	{
		this.server = server;
		this.port = port;
	}

	@Override
	public boolean put(K key, V value) throws KVException
	{

	    if(key == null) throw new KVException(new KVMessage("Empty key"));
	    if(value == null) throw new KVException(new KVMessage("Empty value"));
		String newKey = getStringFromObj(key);
		String newValue = getStringFromObj(value);
		if((newKey.length() * 6) > 256 * 8) throw new KVException(new KVMessage("Over sized key"));
		if((newValue.length() * 6) > 128000 * 8) throw new KVException(new KVMessage("Over sized value"));
		KVMessage m = new KVMessage("putreq", newKey, newValue);

		try
		{
			Socket s = new Socket(server, port);

			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
			DataInputStream in = new DataInputStream(s.getInputStream());


			out.println(m.toXML());
			out.flush();

			// Do I need a while loop here to wait till the input stream has something??
			
			while(in.available() < 1)
				Thread.sleep(50);
			
			KVMessage inMessage = new KVMessage(in);

			in.close();
			out.close();
			
			if(!inMessage.msg.equals("Success"))
				throw new KVException(inMessage);
			
			if(inMessage.status.equals("True"))
				return true;
			else
				return false;
		}
		catch (UnknownHostException e) {throw new KVException(new KVMessage("Network Error: Could not connect"));}
		catch (IOException e) {throw new KVException(new KVMessage("Network Error: Could not create socket"));} 
		catch (KVException e) {throw e;}
		catch (Exception e) {}
		
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) throws KVException
	{
		boolean fail = false;
		if(key == null) throw new KVException(new KVMessage("Empty key"));
		String newKey = getStringFromObj(key);
		if((newKey.length() * 6) > 256 * 8) throw new KVException(new KVMessage("Over sized key"));

		KVMessage m = new KVMessage("getreq", newKey, "");

		try

		{
			Socket s = new Socket(server, port);
			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
			DataInputStream in = new DataInputStream(s.getInputStream());

			out.println(m.toXML());

			while(in.available() < 1)
				Thread.sleep(50);
			KVMessage inMessage = new KVMessage(in);

			in.close();
			out.close();
			if(inMessage.getKey().trim().equals("null")){
				throw new KVException(inMessage);
			}
			
			
			return (V)getObjFromString(inMessage.getValue());
		}
		catch (UnknownHostException e) {throw new KVException(new KVMessage("Network Error: Could not connect"));}
		catch (IOException e) {e.printStackTrace();throw new KVException(new KVMessage("Network Error: Could not create socket"));}
		catch (KVException e) {throw e;}
		catch (Exception e) {}
		

		return null;
	}

	@Override
	public void del(K key) throws KVException {

	    if(key == null) throw new KVException(new KVMessage("Empty key"));
		String newKey = getStringFromObj(key);
		if((newKey.length() * 6) > 128 * 8) throw new KVException(new KVMessage("Over sized key"));

		KVMessage m = new KVMessage("delreq", newKey, "");

		try

		{
			Socket s = new Socket(server, port);
			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
			DataInputStream in = new DataInputStream(s.getInputStream());

			out.println(m.toXML());
			out.flush();

			while(in.available() < 1)
				Thread.sleep(50);
			KVMessage inMessage = new KVMessage(in);


			// if(inMessage could not find object)
			//throw IOException

			in.close();
			out.close();
			
			System.out.println(inMessage.msg);
			
			if(!inMessage.msg.matches("Success"))
			{
				throw new KVException(inMessage);
			}

		}
		catch (UnknownHostException e) {throw new KVException(new KVMessage("Network Error: Could not connect"));}
		catch (IOException e) {throw new KVException(new KVMessage("Network Error: Could not create socket"));}
		catch(KVException e) {throw e;}
		catch (Exception e) {e.printStackTrace();}
	}

	public Object getObjFromString(String o) throws Exception
	{
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] decodedBytes = decoder.decodeBuffer(o);
		
	    ObjectInputStream objIn = new ObjectInputStream(new ByteArrayInputStream(decodedBytes));
	    
		return objIn.readObject();
	}

	public String getStringFromObj(Object o) throws KVException
	{
		byte str[] = null;

		ByteArrayOutputStream b = new ByteArrayOutputStream();

		try
		{
			ObjectOutputStream out = new ObjectOutputStream(b);
			out.writeObject(o);
			out.flush();
			str = b.toByteArray();
		}
		catch(Exception e){}
		
		BASE64Encoder encoder = new BASE64Encoder();

		return 	encoder.encode(str);
	}
}
